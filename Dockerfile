FROM docker:23.0.1

ARG ARCH_CMD='eval uname -m | grep -q x86_64 && echo "amd64" || echo "arm64"'

RUN apk upgrade

RUN apk add --no-cache \
    git \
    openssh  \
    bash \
    gnupg \
    pass

ENV USER=root
ENV HOME=/root

RUN if [[ "$($ARCH_CMD)" == "arm64" ]]; then  \
      wget "https://github.com/docker/docker-credential-helpers/releases/download/v0.7.0/docker-credential-pass-v0.7.0.linux-arm64" -O /usr/local/bin/docker-credential-pass \
      && chmod +x /usr/local/bin/docker-credential-pass; \
    else \
     wget "https://github.com/docker/docker-credential-helpers/releases/download/v0.6.3/docker-credential-pass-v0.6.3-amd64.tar.gz" \
     && tar -xf "docker-credential-pass-v0.6.3-amd64.tar.gz" \
     && chmod +x docker-credential-pass \
     && mv docker-credential-pass /usr/local/bin/ \
     && rm "docker-credential-pass-v0.6.3-amd64.tar.gz"; \
    fi;

COPY config.json $HOME/.docker/

# Create the .gnupg directory and set the correct permissions
RUN mkdir -p $HOME/.gnupg/ \
    && chmod 700 $HOME/.gnupg

WORKDIR $HOME
USER $USER

COPY gpg_file.txt .

# Edit the gpg file to add our password and generate the key
RUN --mount=type=secret,id=gpg_passphrase cat gpg_file.txt | sed 's/gpg_passphrase/'"`cat /run/secrets/gpg_passphrase`"'/g' | gpg --batch --generate-key

# Generate the pass store by accessing and passing the gpg fingerprint
RUN pass init $(gpg --list-secret-keys gitlab@cryptexlabs.com | sed -n '/sec/{n;p}' | sed 's/^[[:space:]]*//g')

RUN password_len=20 \
    && pass_password="$(openssl rand -base64 $password_len | tr -dc 'a-zA-Z0-9' | fold -w $password_len | head -n 1)" \
    && echo -n "$pass_password" | PASSWORD_STORE_X_SELECTION=primary pass insert --force --multiline docker-credential-helpers/docker-pass-initialized-check

RUN --mount=type=secret,id=gitlab_registry_token,uid=1001 cat /run/secrets/gitlab_registry_token | docker login registry.gitlab.com --username joshwoodcock --password-stdin
RUN --mount=type=secret,id=docker_io_registry_token,uid=1001 cat /run/secrets/docker_io_registry_token | docker login --username cryptexlabsgitlab --password-stdin
